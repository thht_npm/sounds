import { FromArray } from './from_array';

class SumSound extends FromArray {
    /**
     * Add both sounds. Must be of equal length and channel count.
     *
     * Please note that amplification will be applied to both sounds.
     *
     * @param {Base} sound_a - The first sound to mix.
     * @param {Base} sound_b - The second sound to mix.
     */
    constructor(sound_a, sound_b) {
        sound_a.apply_amplification();
        sound_b.apply_amplification();

        const new_sound_data = new Array(sound_a.n_channels);
        for (let idx_chan = 0; idx_chan < sound_a.n_channels; idx_chan += 1) {
            new_sound_data[idx_chan] = new Array(sound_a.n_samples);
            const channel_data_a = sound_a.webaudio_buffer.getChannelData([idx_chan]);
            const channel_data_b = sound_b.webaudio_buffer.getChannelData([idx_chan]);

            for (let idx_sample = 0; idx_sample < sound_a.n_samples; idx_sample += 1) {
                // eslint-disable-next-line max-len
                new_sound_data[idx_chan][idx_sample] = channel_data_a[idx_sample] + channel_data_b[idx_sample];
            }
        }

        super(new_sound_data, sound_a.s_freq);
    }
}

export { SumSound };
