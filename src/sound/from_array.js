import { Base } from './base';

class FromArray extends Base {
    /**
     * Generate a sound from a mathjs.matrix.
     * @extends Base
     * @param {Array} sound_data - The sound data
     * @param {number} s_freq - The sampling frequency of the sound data.
     */
    constructor(sound_data, s_freq) {
        super();
        this.set_sound_data(sound_data, s_freq);
    }
}

export { FromArray };
