import { FFT } from 'dsp.js';
import Fili from 'fili';
import { range } from '../lib/mathjs_extra';

let ThisAudioContext = null;
if (typeof window === 'undefined' || typeof AudioContext === 'undefined') {
    // eslint-disable-next-line global-require,import/no-extraneous-dependencies
    ThisAudioContext = require('@descript/web-audio-js').RenderingAudioContext;
} else {
    ThisAudioContext = AudioContext;
}

class Base {
    /**
     * Base Sound class.
     *
     * All other sound classes inherit all properties and methods of this class.
     *
     */
    constructor() {
        this._amplification_factor = null;
        this._internal_webaudio_context = null;
        this._webaudio_buffer = null;
        this._amplification_applied = true;
        this._base_rms = null;
        this._base_absmax = null;
        this._last_buffersource = null;

        /**
         * Ignore clipping error if set to `true`.
         *
         * @type {boolean}
         */
        this.ignore_clipping_error = false;
    }

    /**
     * The webaudio context used for playing. If jsPsych_ is installed and no context is set
     * by the user, the one by jsPsych_ will be used.
     *
     * @type object
     */
    get webaudio_context() {
        if (this._internal_webaudio_context !== null) {
            return this._internal_webaudio_context;
        }

        if (Base._global_audio_context === null) {
            let ctx = null;
            if (typeof (jsPsych) !== 'undefined') {
                // eslint-disable-next-line no-undef
                ctx = jsPsych.pluginAPI.audioContext();
            }
            if (ctx === null) {
                ctx = new ThisAudioContext();
            }
            if (ctx === null) {
                throw new Error('No webaudio context provided and jsPsych not yet initialized.');
            }
            Base._global_audio_context = ctx;
        }
        return Base._global_audio_context;
    }

    /**
     * The duration of the sound in seconds.
     * @type number
     * @readonly
     */
    get duration() {
        if (this._webaudio_buffer === null) {
            return 0;
        }

        return this._webaudio_buffer.duration;
    }

    /**
     * The number of samples
     * @type number
     * @readonly
     */
    get n_samples() {
        if (this._webaudio_buffer === null) {
            return 0;
        }
        return this._webaudio_buffer.length;
    }

    /**
     * The number of channels
     * @type number
     * @readonly
     */
    get n_channels() {
        if (this._webaudio_buffer === null) {
            return 0;
        }
        return this._webaudio_buffer.numberOfChannels;
    }

    set webaudio_context(ctx) {
        this._internal_webaudio_context = ctx;
    }

    set_sound_data(data, s_freq) {
        let this_data = data;

        if (!Array.isArray(data[0]) && !ArrayBuffer.isView(data[0])) {
            this_data = [data];
        }
        if (this.s_freq !== s_freq
            || this.n_channels !== this_data.length
            || this.n_samples !== this_data[0].length) {
            this._webaudio_buffer = this.webaudio_context.createBuffer(
                this_data.length,
                this_data[0].length,
                s_freq,
            );
        }

        this_data.forEach((this_channel, idx_chan) => {
            this._webaudio_buffer.copyToChannel(new Float32Array(this_channel), idx_chan);
        });

        this._amplification_applied = true;
        this._amplification_factor = new Array(this.n_channels).fill(1);
        this._base_absmax = null;
        this._base_rms = null;
    }

    /**
     * The sampling rate of the sound.
     * @type number
     * @readonly
     */
    get s_freq() {
        if (this._webaudio_buffer === null) {
            return 0;
        }

        return this._webaudio_buffer.sampleRate;
    }

    /**
     * The factor by which the raw sound data is to be amplified.
     * @type number
     */
    get amplification_factor() {
        return this._amplification_factor;
    }

    set amplification_factor(value) {
        const this_value = this._check_param(value);

        const is_old = this_value.every((x, idx) => x === this._amplification_factor[idx]);

        if (!is_old) {
            if (!this.ignore_clipping_error) {
                const no_clipping = this_value.every((x, idx) => x * this.base_absmax[idx] <= 1);
                if (!no_clipping) {
                    throw Error('Sound amplification too high. This would lead to clipping');
                }
            }

            this._amplification_factor = this_value;
            this._amplification_applied = false;
        }
    }

    get base_absmax() {
        if (this._base_absmax === null) {
            this._base_absmax = new Array(this.n_channels);
            for (let idx_chan = 0; idx_chan < this.n_channels; idx_chan += 1) {
                let cur_maxabs = 0;
                this._webaudio_buffer.getChannelData(idx_chan).forEach((val) => {
                    cur_maxabs = Math.max(cur_maxabs, Math.abs(val));
                });
                this._base_absmax[idx_chan] = cur_maxabs;
            }
        }

        return this._base_absmax;
    }

    get base_rms() {
        if (this._base_rms === null) {
            this._base_rms = new Array(this.n_channels);
            for (let idx_chan = 0; idx_chan < this.n_channels; idx_chan += 1) {
                const cur_rms = Math.sqrt(
                    this._webaudio_buffer.getChannelData(idx_chan)
                        .map((x) => x ** 2)
                        .reduce((a, b) => a + b, 0) / this.n_samples,
                );

                this._base_rms[idx_chan] = cur_rms;
            }
        }

        return this._base_rms;
    }

    /**
     * The root-mean-square of the sound.
     * @type number
     */
    get rms() {
        return this.base_rms.map((rms, idx) => rms * this._amplification_factor[idx]);
    }

    set rms(rms) {
        const this_rms = this._check_param(rms);
        this.amplify(this_rms.map((x, idx) => x / this.rms[idx]));
    }

    /**
     * The absolute maximum of all samples in the sound.
     * @type number
     */
    get absmax() {
        return this.base_absmax.map((absmax, idx) => absmax * this._amplification_factor[idx]);
    }

    set absmax(value) {
        const this_value = this._check_param(value);
        const this_amp = this_value.map((x, idx) => x / this.absmax[idx]);
        this.amplify(this_amp);
    }

    /**
     * The absolute maximum of the sound in dB.
     * @type number
     */
    get db() {
        return this.absmax.map((x) => Math.log10(x) * 20);
    }

    set db(value) {
        const this_db = this._check_param(value);
        this.absmax = this_db.map((x) => 10 ** (x / 20));
    }

    /**
     * The rms of the sound in dB.
     * @type number
     */
    get rms_db() {
        return this.rms.map((x) => Math.log10(x) * 20);
    }

    set rms_db(value) {
        const this_value = this._check_param(value);
        this.rms = this_value.map((x) => 10 ** (x / 20));
    }

    /**
     * Amplify the sound by factor.
     * @param {number} factor - The amplification factor.
     */
    amplify(factor) {
        const this_factor = this._check_param(factor);
        const new_amp = this.amplification_factor.map((x, idx) => x * this_factor[idx]);
        this.amplification_factor = new_amp;
    }

    /**
     * Amplify the sound by dB.
     * @param {number} db - The amount of dB to add.
     */
    amplify_db(db) {
        const this_db = this._check_param(db);
        const factor = this_db.map((x) => 10 ** (x / 20));

        this.amplify(factor);
    }

    /**
     * The webaudio buffer of this sound.
     *
     * It is only calculated the first time and then cached.
     *
     * @type object
     */
    get webaudio_buffer() {
        return this._webaudio_buffer;
    }

    /**
     * A new webaudio buffer source ready for playing.
     *
     * @type AudioBufferSourceNode
     */
    get webaudio_buffersource() {
        const this_buffersource = this.webaudio_context.createBufferSource();
        this_buffersource.buffer = this.webaudio_buffer;
        this_buffersource.connect(this.webaudio_context.destination);

        this._last_buffersource = this_buffersource;

        return this_buffersource;
    }

    /**
     * The last created buffersource.
     *
     * @type AudioBufferSourceNode
     */
    get last_webaudio_buffersource() {
        return this._last_buffersource;
    }

    /**
     * The sound data of the sound as an array. If you change this, you change the sound!
     *
     * @type Array
     */
    get sound_data_array() {
        const sound_data = Array(this.n_channels);
        for (let idx_chan = 0; idx_chan < this.n_channels; idx_chan += 1) {
            sound_data[idx_chan] = this.webaudio_buffer.getChannelData(idx_chan);
        }

        return sound_data;
    }

    /**
     * Play the sound.
     *
     * @param {number} [start_time=-1] - When to start relative to the
     *                                   time of the current webaudio context.
     * @returns {AudioBufferSourceNode} The webaudio buffer node.
     */
    start(start_time = -1) {
        this.apply_amplification();

        let this_start_time = start_time;

        if (start_time === -1) {
            this_start_time = this.webaudio_context.currentTime;
        }

        const buffer_source = this.webaudio_buffersource;
        buffer_source.start(this_start_time);
        return buffer_source;
    }

    /**
     * Apply a sine ramp to both ends of the sound.
     *
     * @param {number} duration - Duration of the ramp in seconds.
     */
    apply_sin_ramp(duration) {
        const nr = Math.floor(this.s_freq * duration);
        const ramp = range(0, Math.PI / 2, (Math.PI / 2) / nr).map((x) => Math.sin(x));

        for (let idx_chan = 0; idx_chan < this.n_channels; idx_chan += 1) {
            const cur_data = this._webaudio_buffer.getChannelData(idx_chan);
            ramp.forEach((val, idx) => {
                cur_data[idx] *= val;
                const back_idx = cur_data.length - (idx + 1);
                cur_data[back_idx] *= val;
            });
        }

        this._base_absmax = null;
        this._base_rms = null;
    }

    apply_amplification() {
        if (!this._amplification_applied) {
            for (let idx_chan = 0; idx_chan < this.n_channels; idx_chan += 1) {
                const cur_data = this._webaudio_buffer.getChannelData(idx_chan);
                // eslint-disable-next-line no-return-assign
                cur_data.forEach((v, idx) => cur_data[idx] *= this.amplification_factor[idx_chan]);
            }

            this._amplification_applied = true;
            this._amplification_factor = new Array(this.n_channels).fill(1);
            this._base_absmax = null;
            this._base_rms = null;
        }
    }

    /**
     * Calculate FFT Spectrum
     *
     * @returns {FFT} The FFT Spectrum
     */
    get_spectrum() {
        const final_size = 2 ** (Math.ceil(Math.log(this.n_samples) / Math.log(2)));

        let pad_array = new Float32Array(final_size - this.n_samples);
        pad_array = pad_array.fill(0);

        const data_for_fft = new Float32Array(final_size);

        const fft_spec = [];
        this.sound_data_array.forEach((row) => {
            data_for_fft.set(row);
            data_for_fft.set(pad_array, this.n_samples);

            const fft = new FFT(final_size, this.s_freq);
            fft.forward(data_for_fft);

            fft_spec.push(fft);
        });

        return fft_spec;
    }

    /**
     * Apply fili filter to the data
     *
     * @param filter {IIRFilter | FIRFilter} - The filter to apply
     */
    apply_fili_filter(filter) {
        const sound_data = this.sound_data_array;

        sound_data.forEach((val) => {
            if (typeof filter.initZero === 'function') {
                filter.initZero();
            } else if (typeof filter.reinit === 'function') {
                filter.reinit();
            }
            filter.filtfilt(val, true);
        });
    }

    /**
     * Apply an FIR lowpass filter.
     * @param freq {number} - Edge Frequency
     * @param {number} [order=100] - Filter order
     *
     * @returns {FirFilter} The applied filter
     */
    apply_lowpass(freq, order = 100) {
        const fircalc = new Fili.FirCoeffs();
        const filter_coeffs = fircalc.lowpass({
            order,
            Fs: this.s_freq,
            Fc: freq,
        });

        const fir_filter = new Fili.FirFilter(filter_coeffs);

        this.apply_fili_filter(fir_filter);

        return fir_filter;
    }

    /**
     * Apply an FIR highpass filter.
     * @param freq {number} - Edge Frequency
     * @param {number} [order=100] - Filter order
     *
     * @returns {FirFilter} The applied filter
     */
    apply_highpass(freq, order = 100) {
        const fircalc = new Fili.FirCoeffs();
        const filter_coeffs = fircalc.highpass({
            order,
            Fs: this.s_freq,
            Fc: freq,
        });

        const fir_filter = new Fili.FirFilter(filter_coeffs);

        this.apply_fili_filter(fir_filter);

        return fir_filter;
    }

    /**
     * Apply bandpass filter
     * @param low_freq {number} - Low edge frequency
     * @param high_freq {number} - High edge frequency
     * @param {number} [order=100] - Filter order
     *
     * @returns {FirFilter} The applied filter
     */
    apply_bandpass(low_freq, high_freq, order = 100) {
        const fircalc = new Fili.FirCoeffs();
        const filter_coeffs = fircalc.bandpass({
            order,
            Fs: this.s_freq,
            F1: low_freq,
            F2: high_freq,
        });

        const fir_filter = new Fili.FirFilter(filter_coeffs);

        this.apply_fili_filter(fir_filter);

        return fir_filter;
    }

    /**
     * Apply bandstop filter
     * @param low_freq {number} - Low edge frequency
     * @param high_freq {number} - High edge frequency
     * @param {number} [order=100] - Filter order
     *
     * @returns {FirFilter} The applied filter
     */
    apply_bandstop(low_freq, high_freq, order = 100) {
        const fircalc = new Fili.FirCoeffs();
        const filter_coeffs = fircalc.bandstop({
            order,
            Fs: this.s_freq,
            F2: low_freq,
            F1: high_freq,
        });

        const fir_filter = new Fili.FirFilter(filter_coeffs);

        this.apply_fili_filter(fir_filter);

        return fir_filter;
    }

    _check_param(value) {
        if (!Array.isArray(value)) {
            return new Array(this.n_channels).fill(value);
        }
        return value;
    }
}

Base._global_audio_context = null;

export { Base };
