import { FromArray } from './from_array';
import { range } from '../lib/mathjs_extra';

class Sine extends FromArray {
    /**
     * Generate a sine wave.
     *
     * @param {number} freq - The frequency of the sine wave.
     * @param {number} duration - The duration in seconds.
     */
    constructor(freq, duration) {
        const s_freq = 44100;
        const n_samples = Math.floor(duration * s_freq);

        const factor = 2 * Math.PI * (freq / s_freq);
        const tmp_s_idx = range(0, n_samples).map((x) => x * factor);
        const sound_data = tmp_s_idx.map((x) => Math.sin(x));

        super(sound_data, s_freq);
    }
}

export { Sine };
