import { FromArray } from './from_array';

class Noise extends FromArray {
    /**
     * Generate white noise.
     *
     * @param {number} duration - The duration in seconds.
     */
    constructor(duration) {
        const s_freq = 44100;
        const n_samples = Math.floor(duration * s_freq);
        const sound_data = Array.from({ length: n_samples }, () => (2 * Math.random()) - 1);

        super(sound_data, s_freq);
    }
}

export { Noise };
