import { Base } from './base';

class Soundfile extends Base {
    /**
     * Load sound from a URL pointing to a soundfile.
     *
     * Please be aware that creating an instance of this class does not actually load the sound.
     * Before it can be used, you need to call the load function which returns a Promise.
     *
     * Refer to :ref:`play_sound_from_files` to find out more.
     *
     * @extends Base
     * @param url {String} - The url of the soundfile
     */
    constructor(url) {
        super();
        this._url = url;
    }

    /**
     * Actually load the sound file.
     * @returns {Promise} Empty Promise object
     */
    async load() {
        const response = await fetch(this._url);
        const tmp_data = await response.arrayBuffer();
        this._webaudio_buffer = await this.webaudio_context.decodeAudioData(tmp_data);

        this._amplification_applied = true;
        this._amplification_factor = new Array(this.n_channels).fill(1);
        this._base_absmax = null;
        this._base_rms = null;
    }
}

export { Soundfile };
