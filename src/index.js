import { Base } from './sound/base';
import { FromArray } from './sound/from_array';
import { Noise } from './sound/noise';
import { Sine } from './sound/sine';
import { SumSound } from './sound/sum';
import { Soundfile } from './sound/soundfile';

export {
    Base, FromArray, Noise, Sine, SumSound, Soundfile,
};
