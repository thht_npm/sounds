import { plot } from 'nodeplotlib';

/**
 * Plot a fili filter in the webbrowser.
 *
 * @param filter {Filter} - The fili filter to plot
 * @param fs {number} - The sampling rate
 */
function plot_filter(filter, fs) {
    const resp = filter.response(fs);
    const db_resp = resp.map((r) => r.dBmagnitude);
    const freqs = resp.map((r, idx) => idx);

    const plot_data = {
        x: [...freqs],
        y: [...db_resp],
        type: 'line',
    };

    plot([plot_data]);
}

export { plot_filter };
