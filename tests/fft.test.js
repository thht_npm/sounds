import { Sine } from '../src';

test('Get Spectrum', () => {
    const snd = new Sine(440, 1);
    expect(snd).not.toBeNull();

    const spectrum = snd.get_spectrum();

    spectrum.forEach((spec) => {
        expect(spec.getBandFrequency(spec.peakBand)).toBeCloseTo(440, 0);
    });
});
