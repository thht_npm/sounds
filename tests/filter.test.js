// eslint-disable-next-line no-unused-vars
import { plot } from 'nodeplotlib';
import { Noise } from '../src';

function get_spectrum_freqs(spectrum) {
    return spectrum.spectrum.map((val, idx) => spectrum.getBandFrequency(idx));
}

function get_idx_for_freq(freq, spectrum) {
    const freqs = get_spectrum_freqs(spectrum);
    const diff = freqs.map((val) => Math.abs(val - freq));

    return diff.indexOf(Math.min(...diff));
}

function get_power_at_freq(freq, spectrum) {
    const idx = get_idx_for_freq(freq, spectrum);

    return spectrum.spectrum[idx];
}

// eslint-disable-next-line no-unused-vars
function get_node_spectrum_plot(spectrum) {
    return {
        x: [...get_spectrum_freqs(spectrum)],
        y: [...spectrum.spectrum],
        type: 'line',
    };
}

test('Lowpass filter', () => {
    const lp_freq = 1000;
    const noise = new Noise(1);
    noise.apply_sin_ramp(0.01);
    noise.apply_lowpass(lp_freq);

    const spectra = noise.get_spectrum();

    spectra.forEach((spec) => {
        const power_low = get_power_at_freq(200, spec);
        const power_high = get_power_at_freq(2000, spec);

        expect(power_low).toBeGreaterThan(power_high);
    });
});

test('Highpass filter', () => {
    const hp_freq = 1000;
    const noise = new Noise(1);
    noise.apply_sin_ramp(0.01);
    noise.apply_highpass(hp_freq);

    const spectra = noise.get_spectrum();

    spectra.forEach((spec) => {
        const power_low = get_power_at_freq(200, spec);
        const power_high = get_power_at_freq(2000, spec);

        expect(power_low).toBeLessThan(power_high);
    });
});

test('Bandpass filter', () => {
    const low_freq = 1000;
    const high_freq = 2000;

    const noise = new Noise(1);
    noise.apply_sin_ramp(0.01);
    noise.apply_bandpass(low_freq, high_freq);

    const spectra = noise.get_spectrum();

    spectra.forEach((spec) => {
        const power_low = get_power_at_freq(200, spec);
        const power_middle = get_power_at_freq(1500, spec);
        const power_high = get_power_at_freq(4000, spec);

        expect(power_low).toBeLessThan(power_middle);
        expect(power_high).toBeLessThan(power_middle);
    });
});

test('Bandstop filter', () => {
    const low_freq = 1000;
    const high_freq = 2000;

    const noise = new Noise(1);
    noise.apply_sin_ramp(0.01);
    noise.apply_bandstop(low_freq, high_freq, 100);

    const spectra = noise.get_spectrum();

    spectra.forEach((spec) => {
        const power_low = get_power_at_freq(200, spec);
        const power_middle = get_power_at_freq(1500, spec);
        const power_high = get_power_at_freq(4000, spec);

        expect(power_low).toBeGreaterThan(power_middle);
        expect(power_high).toBeGreaterThan(power_middle);
    });
});
