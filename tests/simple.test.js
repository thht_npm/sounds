import { FFT } from 'dsp.js';
import {
    FromArray, Noise, Sine, SumSound,
} from '../src';
import { range } from '../src/lib/mathjs_extra';

test('Get simple sine wave', () => {
    const sin = new Sine(440, 1);
    sin.absmax = 0.7;
    expect(sin.absmax[0]).toBe(0.7);
    sin.db = -20;
    expect(sin.db[0]).toBeCloseTo(-20);
    sin.amplify_db(-20);
    expect(sin.db[0]).toBe(-40);
    sin.rms_db = -30;
    expect(sin.rms_db[0]).toBe(-30);
    sin.amplify_db(10);
    expect(sin.rms_db[0]).toBe(-20);
    sin.rms = 0.4;
    expect(sin.rms[0]).toBe(0.4);
});

test('Get simple noise', () => {
    const noise = new Noise(1.3);
    noise.rms = 0.1;
    expect(noise.rms[0]).toBeCloseTo(0.1);
});

test('Create a valid summed sound', () => {
    const sin = new Sine(440, 1);
    const sin2 = new Sine(550, 1);

    sin.absmax = 0.3;
    sin2.absmax = 0.2;

    const combined = new SumSound(sin, sin2);
    expect(combined.duration).toBe(1);
});

test('Test sin ramp', () => {
    const n_samples = 44100;
    const s_rate = 44100;
    const sin_ramp_duration = 0.05;
    const my_ones = Array(2).fill(Array(n_samples).fill(1));
    const sin_ramp_n_samples = Math.floor(n_samples * sin_ramp_duration);

    const my_sound = new FromArray(my_ones, s_rate);
    my_sound.apply_sin_ramp(sin_ramp_duration);

    const final_sound_data = my_sound.sound_data_array;

    expect(final_sound_data[0][0]).toBe(0);
    expect(final_sound_data[0][final_sound_data[0].length - 1]).toBe(0);
    expect(final_sound_data[1][n_samples / 2]).toBe(1);

    let onset_sin = final_sound_data[0].slice(0, sin_ramp_n_samples + 1);
    expect(onset_sin[0]).toBe(0);
    expect(onset_sin[onset_sin.length - 1]).toBe(1);

    onset_sin = onset_sin.slice(0, -1);

    const compare_sin = range(
        0, Math.PI / 2, (Math.PI / 2) / onset_sin.length,
    ).map((x) => Math.sin(x));
    compare_sin.forEach((x, idx) => expect(x).toBeCloseTo(onset_sin[idx]));

    compare_sin.reverse();
    const offset_sin = final_sound_data[0].slice(-sin_ramp_n_samples);
    compare_sin.forEach((x, idx) => expect(x).toBeCloseTo(offset_sin[idx]));
});

test('Play a sound', () => {
    const sine_wave = new Sine(440, 2);
    const audio_ctx = sine_wave.webaudio_context;

    sine_wave.start();
    audio_ctx.processTo(1);

    const audio_data = audio_ctx.exportAsAudioData();
    const tmp_data_for_fft = audio_data.channelData[0].filter(
        (value) => !(Number.isNaN(value)),
    );

    const final_size = 2 ** (Math.ceil(Math.log(tmp_data_for_fft.length) / Math.log(2)));

    let pad_array = new Float32Array(final_size - tmp_data_for_fft.length);
    pad_array = pad_array.fill(0);

    const data_for_fft = new Float32Array(final_size);
    data_for_fft.set(tmp_data_for_fft);
    data_for_fft.set(pad_array, tmp_data_for_fft.length);

    const fft = new FFT(final_size, audio_data.sampleRate);
    fft.forward(data_for_fft);

    expect(fft.getBandFrequency(fft.peakBand)).toBeCloseTo(440, 0);
});

test('Throw exceptions when clipping', () => {
    const error_message = 'Sound amplification too high. This would lead to clipping';
    const snd = new Sine(440, 0.1);

    expect(() => { snd.absmax = 1.01; }).toThrow(error_message);
    expect(() => { snd.db = 0.01; }).toThrow(error_message);
    expect(() => { snd.rms += 0.1; }).toThrow(error_message);
});

test('Ignore clipping exception', () => {
    const snd = new Sine(440, 0.1);
    snd.ignore_clipping_error = true;

    expect(() => { snd.absmax = 1.01; }).not.toThrow();
});

test('Always the same AudioContext', () => {
    const snd1 = new Sine(440, 0.1);
    const snd2 = new Noise(0.2);

    expect(snd1.webaudio_context).toEqual(snd2.webaudio_context);
});
