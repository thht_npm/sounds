Howto install and use it
========================

Install
-------

@thht/sounds is provided as a npm package:

.. code-block:: bash

    npm install @thht/sounds

Use it
------

You can now use any of the sound classes provided by @thht/sounds. First import the one(s) that you are going to use:

.. code-block:: javascript

    import { Sine } from "@thht/sounds"

Then create an object of the class and play around with it:

.. code-block:: javascript

    const my_sine = new Sine(440, 1.2);
    my_sine.apply_sin_ramp(0.01);

    // get webaudio context
    window.AudioContext = window.AudioContext||window.webkitAudioContext;
    const context = new AudioContext();

    my_sine.webaudio_context = context;

    // play it
    my_sine.start();

Adjusting the volume
++++++++++++++++++++

@thht/sounds objects have multiple parameters representing the current volume of the sound:

1. amplification_factor
2. rms
3. absmax
4. db
5. rms_db

These properties are all interconnected. If you set one of them, the value of the others will change accordingly.

If you try to amplify a sound to a volume that would lead to clipping, an exception is thrown.

.. _play_sound_from_files:

Play a sound from a wav or mp3 file
+++++++++++++++++++++++++++++++++++

Loading sounds from files is a bit special compared to using sine waves, noise or supplying an array of sound data.
Loading and decoding of sound files might take some time and the way it is done in your browser makes sure that the
browser does not block. Unfortunately, this makes things a bit more complicated for us here.

When you create a sound object of the :class:`Soundfile` class, you cannot play it right away. Instead, you need to
call the :meth:`Soundfile.load` method in order to actually load and decode the soundfile.

The problem is that :meth:`Soundfile.load` is an *asynchronous* method. This means that we **cannot** write
code like this:

.. code-block:: javascript

    const my_mp3 = new Soundfile(mp3_url);
    my_mp3.load();
    my_mp3.start();

The command :code:`my_mp3.load();` tells the browser to start loading and decoding the soundfile but then **does not
wait for it to finish**. Instead, it returns immediately so :code:`my_mp3.start();` would get executed before the
sound is actually ready to be played.

Instead, we need to tell the browser: As soon as you are done loading and decoding, execute this function which then
takes care of starting the playback:

.. code-block:: javascript

    const my_mp3 = new Soundfile(mp3_url);
    my_mp3.load()
        .then(() => my_mp3.start());

If you use jsPsych, you might want to do this before calling :code:`jsPsych.init`, i.e. put that call in the
:code:`then` function.

Handling multiple sound files
+++++++++++++++++++++++++++++

Using the above approach becomes quite tedious if you need to work with multiple files because then you would need
to chain it like this:

.. code-block:: javascript

    const my_mp3_1 = new Soundfile(mp3_url_1);
    const my_mp3_2 = new Soundfile(mp3_url_2);
    const my_mp3_3 = new Soundfile(mp3_url_3);
    my_mp3_1.load()
        .then(() => my_mp3_2.load())
        .then(() => my_mp3_3.load())
        .then(() => {
            my_mp3_1.start();
            my_mp3_2.start();
            my_mp3_3.start();
        });

Because the :meth:`Soundfile.load` method returns a so-called `Promise <https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise>`_,
we can simplify it like this:

.. code-block:: javascript

    const my_mp3_1 = new Soundfile(mp3_url_1);
    const my_mp3_2 = new Soundfile(mp3_url_2);
    const my_mp3_3 = new Soundfile(mp3_url_3);

    Promise.all([my_mp3_1.load(), my_mp3_2.load(), my_mp3_3.load()])
        .then(() => {
            my_mp3_1.start();
            my_mp3_2.start();
            my_mp3_3.start();
        });

:code:`Promise.all` takes an array of Promises and resolves (i.e. calls the function supplied at the :code:`then`
method) when all Promises have resolved, in our case when all sound files have finished loading and decoding.

More information
----------------

For more information, please refer to the :doc:`reference`.
