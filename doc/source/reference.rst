Reference
=========

Specific Sound classes
----------------------

.. autoclass:: Sine
.. autoclass:: Noise
.. autoclass:: SumSound
.. autoclass:: Soundfile

    .. autofunction:: Soundfile#load
        :short-name:

The base classes
----------------

.. autoclass:: Base

    .. rubric:: Play the sound
    .. autofunction:: Base#start
        :short-name:

    .. rubric:: Process the sound
    .. autofunction:: Base#apply_sin_ramp
        :short-name:
    .. autofunction:: Base#apply_fili_filter
        :short-name:
    .. autofunction:: Base#apply_lowpass
        :short-name:
    .. autofunction:: Base#apply_highpass
        :short-name:
    .. autofunction:: Base#apply_bandpass
        :short-name:
    .. autofunction:: Base#apply_bandstop
        :short-name:

    .. rubric:: Interact with webaudio
    .. autoattribute:: Base#webaudio_context
        :short-name:
    .. autoattribute:: Base#webaudio_buffer
        :short-name:
    .. autoattribute:: Base#webaudio_buffersource
        :short-name:
    .. autoattribute:: Base#last_webaudio_buffersource
        :short-name:

    .. rubric:: Properties and methods for getting and setting the volume
    .. autoattribute:: Base#absmax
        :short-name:
    .. autoattribute:: Base#amplification_factor
        :short-name:
    .. autoattribute:: Base#rms
        :short-name:
    .. autoattribute:: Base#db
        :short-name:
    .. autoattribute:: Base#rms_db
        :short-name:
    .. autofunction:: Base#amplify
        :short-name:
    .. autofunction:: Base#amplify_db
        :short-name:

    .. rubric:: General properties
    .. autoattribute:: Base#duration
        :short-name:
    .. autoattribute:: Base#n_samples
        :short-name:
    .. autoattribute:: Base#s_freq
        :short-name:
    .. autoattribute:: Base#ignore_clipping_error
        :short-name:
    .. autoattribute:: Base#sound_data_array
        :short-name:

.. autoclass:: FromArray
    :members:

Helper
------

.. autofunction:: plot_filter
    :short-name:

.. include:: links.rst
