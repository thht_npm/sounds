.. @thht/sounds documentation master file, created by
   sphinx-quickstart on Mon Oct  7 21:45:14 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to @thht/sounds's documentation!
========================================

Introduction
------------

@thht/sounds provides handy javascript sound classes. Sound objects can be conveniently played via webaudio.
Support for jsPsych_ is included, as well.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage
   reference

.. include:: links.rst

