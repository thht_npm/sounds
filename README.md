# Sound classes for Javascript

@thht/sounds provides handy javascript sound classes. Sound objects can be conveniently played via webaudio.
Support for [jsPsych](https://www.jspsych.org/) is included, as well.

The full documentation can be found here: <https://thht_npm.gitlab.io/sounds/>

